
# Docker Development Environment Container (dockerdevenv)

A Docker container that integrates SSHD, Ubuntu and out-of-the-box environment installations with automatic,
configurable and reusable development environments.

The container has [zsh](https://www.zsh.org/), [oh-my-zsh](https://ohmyz.sh/) and
[spaceship-prompt](https://github.com/spaceship-prompt/spaceship-prompt) already
packaged on the image. You can also install your own apt packages on startup.
Nvm and docker-cli installs are also supported.

## Usage

- Start the container
    - using volumes:
        ```bash
        docker run -p 20022:22 -v my-projects:/projects -v my-user:/home/user dockerdevenv
        ```
    - using binds:
        ```bash
        docker run -p 20022:22 -v /path/to/projects:/home/user/projects -v /path/to/.ssh:/home/user/.ssh dockerdevenv
        ```
- Connect to the container
    - using vscode
        - install the remote-ssh extension
        - in your local `.ssh/config`:
            ```
            Host my-dev
            HostName <host ip>
            User user
            Port <container forwarded ssh port>
            ```
- use password `user` or the one you set yourself
- in the container run `dev` to get more help
- you should also probably set up ssh keys


## Configuration
### Environment variables
| Name                     | Default | Description                                                                        |
| ------------------------ | ------- | -----------                                                                        |
| NAME                     |         | custom host/container identifier that is only used for helper/logging purposes     |
| INSTALL                  |         | Select software to install on initialization                                       |
| SSH_PORT                 |         | custom container ssh port that is only used for helper/logging purposes            |
| HOST_IP                  |         | host ip address that is only used for helper/logging purposes                      |
| ROOT_PASSWORD            | `root`  | `root` password                                                                    |
| USER_PASSWORD            | `user`  | `user` password                                                                    |
| SUDO_REQUIRES_ROOT_PW    | `false` | Whether `sudo` requires `root` password instead of user                            |
| ROOT_SSH                 | `false` | Enable SSH login with `root` account                                               |
| ENABLE_VSCODE_TUNNEL     | `false` | Enable vscode-server tunneling, by using this you consent to the required licenses |
| VSCODE_TUNNEL_NAME       |         | the vscode-server tunneling name, defaults to a random name (`--random-name` flag) |

### Paths
| path                      | Description                                                       |
| ------------------------- | ----------------------------------------------------------------- |
| /projects                 | user Projects                                                     |
| /home/user                | User home (will be filled with default content on start)          |
| /artifact                 | used to easily export artifact files from the container           |

### Installing software
You can pre-install software like NodeJS, Python, etc. by setting the `INSTALL` environment variable.
You can install software either by using values from the following list, otherwise they will be installed
using `apt-get install`.
Note that the default node version will be the _first one_ defined.

| Key     | Description                                                  |
| ------- | ------------------------------------------------------------ |
| node-xx | e.g. `node-12` for NodeJS v12 using nvm                      |
| python  | Python3 full development packages                            |
| docker  | Docker CE CLI                                                |

## Using VSCode Remote Tunnels

1. Set the `ENABLE_VSCODE_TUNNEL` (and optionally `VSCODE_TUNNEL_NAME`) environment variables and start the container
    - Note that by enabling this feature, you agree to the the Visual Studio Code Server License Terms
      (https://aka.ms/vscode-server-license) and the Microsoft Privacy Statement (https://privacy.microsoft.com/en-US/privacystatement) as stated in the software.
2. Look at the logs; there will be a link and a code to login to your GitHub account.
3. After loggin in, a URL will be logged that can be used to access your development environment. You can also access the
   environment through the Remote Explorer

### Notes

- Make sure that the container's hostname is consistent
  - `vscode-cli tunnel` seems to send the machines hostname with the token,
    unless the hostname is unchanged the token is useless
- The config is stored in `/home/user/.vscode-cli`, you should make sure it's persistent
