#!/bin/env bash

usage() {
    echo
    echo "Usage: start.sh [--ssh] [docker run args]"
    echo
    echo "Builds and runs a testing container."
    echo
    echo "Options:"
    ARGUSAGE=$(echo -e \
        "   \t--ssh\tUse SSH to connect instead of docker exec. This allows for tmux to work\n" \
        "-h,\t--help\tPrint this text\n")
    column -t -s $'\t' <<< "$ARGUSAGE"
    exit 1
}

ARGS=$(getopt -o h -l ssh,help \
              -n 'start.sh' -- "$@")
if [ $? != 0 ] ; then usage ; fi
eval set -- "$ARGS"

USE_SSH=false
USER_PASSWORD=asd123
while true; do
  case "$1" in
    --ssh ) USE_SSH=true; shift ;;
    -h | --help ) usage; shift ;;
    -- ) shift; break ;;
    * ) break ;;
  esac
done

PROJECTPATH="$( cd -- "$(dirname "$(dirname "$0")")" >/dev/null 2>&1 ; pwd -P )"
cd $PROJECTPATH

PACKAGE_VERSION="$(npm run getversion --silent) [DEVELOPMENT]"
export BUILD_TAG=v$PACKAGE_VERSION
docker build \
    -t dockerdevenv:latest \
    --build-arg="BUILD_TAG=$BUILD_TAG" \
    -f Dockerfile . \
    || exit 1
mkdir -p local-cont

if [[ $USE_SSH == true ]]; then
    DOCKER_ARGS="--rm -d"
else
    DOCKER_ARGS="--rm -it"
fi
TS=$(date -Ins)
run() {
    docker run \
        $DOCKER_ARGS \
        -e USER_PASSWORD=$USER_PASSWORD \
        -e ROOT_PASSWORD=asd132 \
        -e SUDO_REQUIRES_ROOT_PW=false \
        -e ROOT_SSH=true \
        -e NAME=test-rig \
        -e SSH_PORT=2222 \
        -e HOST_IP=ip \
        -e INSTALL="node-16 node-12 ncdu" \
        -e DOCKER_HOST="tcp://192.168.0.204:2377" \
        -e TMUX="value" \
        -p 2222:22 \
        -v $PROJECTPATH/local-cont/home:/home/user \
        -v $PROJECTPATH/local-cont/projects:/projects \
        -v $PROJECTPATH/local-cont/artifact:/artifact \
        $@ \
        dockerdevenv
}

if [[ $USE_SSH == true ]]; then
    CONT_ID=$(run)
    check_running() {
        if [ "$( docker container inspect -f '{{.State.Running}}' $CONT_ID )" != "true" ]; then
            echo "Error starting container. Aborting."
            exit 1
        fi
    }
    check_running
    stop() {
        echo -e "\nstopping..."
        docker container stop $CONT_ID &> /dev/null
    }
    trap "stop" EXIT
    # show logs until started
    STARTED=false
    docker logs $CONT_ID
    while [[ $STARTED == "false" ]]; do
        logs=$(docker logs $CONT_ID --since $TS 2>&1)
        TS=$(date -Ins)
        if [[ -n "$logs" ]]; then
            echo "$logs"
        fi
        if [[ "$logs" =~ "s6-rc: info: service legacy-services successfully started" ]]; then
            echo "started!"
            STARTED=true
        else
            check_running
        fi
        sleep 0.5
    done

    ssh_cmd="ssh user@localhost -p 2222"
    if [[ -z "$(which sshpass)" ]]; then
        echo "sshpass not found. Enter \"$USER_PASSWORD\" as the password"
        $ssh_cmd
    else
        sshpass -p "$USER_PASSWORD" $ssh_cmd
    fi
    stop
else
    run
fi