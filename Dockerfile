FROM ubuntu:24.04

# Timezone setup
ENV TZ=Europe/Helsinki
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# prevent CLI questionnaire during installation
ENV DEBIAN_FRONTEND=noninteractive

# Install things
RUN apt-get update && \
    apt-get install -y \
    git \
    curl \
    nano \
    openssh-server \
    sudo \
    zsh \
    tmux \
    bsdmainutils \
    build-essential \
    libssl-dev \
    libffi-dev \
    xz-utils \
    && apt-get clean

# Install s6-overlay
ADD https://github.com/just-containers/s6-overlay/releases/download/v3.1.3.0/s6-overlay-noarch.tar.xz /tmp
RUN tar -C / -Jxpf /tmp/s6-overlay-noarch.tar.xz
ADD https://github.com/just-containers/s6-overlay/releases/download/v3.1.3.0/s6-overlay-x86_64.tar.xz /tmp
RUN tar -C / -Jxpf /tmp/s6-overlay-x86_64.tar.xz

# Clean up
RUN rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Copy setup script (NOT all scripts yet) to /scripts
COPY ./rootfs/scripts/setup.py ./rootfs/scripts/utils.py /scripts/

# SSHD, root and user setup
RUN mkdir /var/run/sshd \
    && echo 'root:root' | chpasswd \
    && sed -ri 's/UsePAM yes/#UsePAM yes/g' /etc/ssh/sshd_config \
    && chmod -R +x /scripts/* \
    && useradd -ms /bin/zsh user \
    && echo "user:user" | chpasswd \
    && adduser user sudo \
    && mkdir /projects

# Run setup script
RUN sudo -u user zsh -c "cd /scripts && /scripts/setup.py"

# Rename the home folder.
# This will allow us to mount the home folder as a volume.
# If we are not using a volume, then we can just rename the
# folder back at startup.
RUN mv /home/user /home/user.old

COPY ./rootfs /
# we need to chmod /scripts again because it has now all
# the scripts in it
RUN chmod -R +x /scripts/*

VOLUME /home/user
VOLUME /projects

EXPOSE 22

ENV S6_BEHAVIOUR_IF_STAGE2_FAILS=2
ENV S6_CMD_WAIT_FOR_SERVICES_MAXTIME=0
# ENV S6_KILL_GRACETIME=1000

ARG BUILD_TAG
ENV build_tag=$BUILD_TAG

# Run when container starts
ENTRYPOINT [ "/init" ]

# Run on user CLI (e.g. -it or ssh)
CMD /scripts/shell.sh
