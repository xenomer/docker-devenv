#!/bin/env python3

from __future__ import annotations
import subprocess
import re
import os
import os.path
import sys
import selectors


class Colors:
    BLACK = "\033[0;30m"
    RED = "\033[0;31m"
    GREEN = "\033[0;32m"
    BROWN = "\033[0;33m"
    BLUE = "\033[0;34m"
    PURPLE = "\033[0;35m"
    CYAN = "\033[0;36m"
    LIGHT_GRAY = "\033[0;37m"
    DARK_GRAY = "\033[1;30m"
    LIGHT_RED = "\033[1;31m"
    LIGHT_GREEN = "\033[1;32m"
    YELLOW = "\033[1;33m"
    LIGHT_BLUE = "\033[1;34m"
    LIGHT_PURPLE = "\033[1;35m"
    LIGHT_CYAN = "\033[1;36m"
    LIGHT_WHITE = "\033[1;37m"
    BOLD = "\033[1m"
    FAINT = "\033[2m"
    ITALIC = "\033[3m"
    UNDERLINE = "\033[4m"
    BLINK = "\033[5m"
    NEGATIVE = "\033[7m"
    CROSSED = "\033[9m"
    END = "\033[0m"

    # cancel SGR codes if we don't write to a terminal
    if not sys.stdout.isatty() and os.environ.get('TERM') == None:
        for _ in dir():
            if isinstance(_, str) and _[0] != '_':
                locals()[_] = ''


def colorize(value: str, color: Colors) -> str:
    return f'{color}{value}{Colors.END}'


def command(
    cmd: str,
    log: bool = False,
    stdout: bool = False,
    stdout_line_prefix: str = '',
    stderr_line_prefix: str = '',
    return_stderr: bool = False
) -> tuple[str, str] | str:
    if log:
        print('$ ' + cmd)

    process = subprocess.Popen(['zsh', '-c', cmd],
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)

    sel = selectors.DefaultSelector()
    sel.register(process.stdout, selectors.EVENT_READ)
    sel.register(process.stderr, selectors.EVENT_READ)

    has_data = True
    output_stdout = ''
    output_stderr = ''
    is_start = True
    ends_with_newline = False
    # read from stdout and stderr
    while has_data:
        for key, _ in sel.select():
            data: str = key.fileobj.read1().decode()
            # EOF
            if not data:
                has_data = False
                break

            # stdout
            if key.fileobj is process.stdout:
                prefix = stdout_line_prefix
                file = sys.stdout
                output_stdout += data

            # stderr
            else:
                prefix = stderr_line_prefix
                file = sys.stderr
                output_stderr += data

            if stdout:
                if is_start:
                    is_start = False
                    data = prefix + data
                if ends_with_newline:
                    data = prefix + data
                ends_with_newline = data.endswith('\n')
                data = re.sub('\\n$', '', data).replace('\n', '\n' + prefix)
                if ends_with_newline:
                    data += '\n'
                print(data, end="", file=file, flush=True)
    process.communicate()
    if process.returncode != 0:
        raise Exception(
            f'Command {cmd} failed with exit code {process.returncode}', process.returncode)
    if return_stderr:
        return output_stdout, output_stderr
    return output_stdout.rstrip('\n')


def run(
    cmd: str,
    log: bool = True,
    stdout: bool = True,
    allow_failure: bool = False,
    multiline: bool = True,
    user: str = None
):
    if multiline:
        cmd = re.sub(r'\s{2,}', ' ', cmd)
    if user:
        cmd = f'sudo -u {user} zsh -c \'{cmd}\''
    try:
        command(cmd, log=log, stdout=stdout)
    except Exception as e:
        if allow_failure:
            return
        msg, code = e.args
        if msg.startswith('Command '):
            print(f'{Colors.RED}{cmd}: exited with {code}{Colors.END}',
                  file=sys.stderr)
            sys.exit(1)
        else:
            raise e


def print_multiline(string: str):
    for line in string.splitlines():
        print(f'{Colors.FAINT}>{Colors.END} {line}')


def get_start_tag(tag: str) -> str: return f'# >>> {tag} >>>'
def get_end_tag(tag: str) -> str: return f'# <<< {tag} <<<'


def get_block_pattern(
    tag: str) -> str: return f'{get_start_tag(tag)}\\n([\\s\\S]+?)\\n{get_end_tag(tag)}'


def has_tag(value: str, tag: str) -> bool:
    return re.search(get_block_pattern(tag), value, re.MULTILINE | re.IGNORECASE) is not None


def get_tag_content(value: str, tag: str) -> str:
    string = re.search(
        get_block_pattern(tag), value, re.MULTILINE | re.IGNORECASE)
    if string:
        return string.group(1)
    else:
        return ''


def update_tag_content(value: str, tag: str, new_value: str, append: bool = True, replace: bool = True) -> tuple[str, bool]:
    hastag = has_tag(value, tag)
    block_value = f'{get_start_tag(tag)}\n{new_value}\n{get_end_tag(tag)}'
    if not hastag and append:
        return value.rstrip() + f'\n\n{block_value}\n', hastag
    elif hastag and replace:
        string = re.sub(
            get_block_pattern(tag), block_value, value, flags=re.MULTILINE | re.IGNORECASE)
        return string, hastag
    else:
        return value, hastag


def replace_in_file(file: str, old: str | re.Pattern, new: str):
    with open(file, 'r') as f:
        content = f.read()
    if isinstance(old, str):
        old = re.compile(re.escape(old), re.MULTILINE)
    content = old.sub(new, content)
    with open(file, 'w') as f:
        f.write(content)


def exists_dir(path: str) -> bool:
    return os.path.isdir(path)


def exists_file(path: str) -> bool:
    return os.path.isfile(path)


def run_script(
    path: str,
    stdout_line_prefix: str = colorize('> ', Colors.FAINT),
    stderr_line_prefix: str = colorize('> ', Colors.RED)
) -> tuple[str, str]:
    return command(
        f'PYTHONPATH="/scripts" TERM=xterm python3 -u {path}',
        stdout=True,
        stdout_line_prefix=stdout_line_prefix,
        stderr_line_prefix=stderr_line_prefix
    )
