#!/command/with-contenv zsh
set -e

# If we are not running -i in Docker,
# the shell will instantly return, therefore
# exiting the container. This will make sure
# that the CMD instruction will hang in such
# case (usually).
if [[ -z "$TERM" ]]; then
    sleep infinity
else
    /bin/zsh
fi