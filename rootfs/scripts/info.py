#!/bin/env python3
from utils import *
import os.path


def e(key: str, default: str = '', color: str = None):
    value = os.environ.get(key, default)
    return colorize(value, color) if color else value


if __name__ == "__main__":

    print()
    print()
    print()

    print('User information:')
    print()
    print(f'{"Username":>10} {"UID":>6} {"GID":>6}  {"Home":15} Password')
    print(f'{"user":>10} {command("id -u user"):>6} {command("id -g user"):>6}  {"/home/user":15} {e("USER_PASSWORD", "user")}')
    print(f'{"root":>10} {command("id -u root"):>6} {command("id -g root"):>6}  {"/root":15} {e("ROOT_PASSWORD", "root")}')
    print()
    print(
        f'Development environment {e("NAME", command("hostname"), Colors.BLUE)} started.')
    print()

    def print_cmd(desc, cmd):
        print(f'  {colorize(f"{desc:29}", Colors.FAINT)} {cmd}')

    print_cmd('Connect with',
              f'ssh user@{e("HOST_IP", "ip")} -p{e("SSH_PORT", "port")}')
    print_cmd('Generate client SSH keys',
              'ssh-keygen -f ~/.ssh/id_rsa')
    print_cmd('Add SSH keys',
              f'ssh-copy-id -i ~/.ssh/id_rsa.pub user@{e("HOST_IP", "ip")} -p{e("SSH_PORT", "port")}')
    print_cmd('Get started (in container)',
              'dev')
    print()
    print()
    print()
