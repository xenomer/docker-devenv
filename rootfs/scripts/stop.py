#!/bin/env python3
import os.path
from utils import *

custom_script = '/home/user/.dev-stop'
custom_script_name = os.path.basename(custom_script)

if __name__ == "__main__":
    if os.path.isfile(custom_script):
        print('executing', custom_script)
        try:
            run(f'chmod +x {custom_script}', log=False)
            command(
                custom_script,
                stdout=True,
                stdout_line_prefix=colorize('> ', Colors.FAINT),
                stderr_line_prefix=colorize('> ', Colors.RED)
            )
        except Exception as e:
            msg, code = e.args
            print(f'error running {custom_script_name}: {msg}')
            sys.exit(code)
