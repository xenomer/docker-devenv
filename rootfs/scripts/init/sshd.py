#!/bin/env python3
from utils import *
import os
import os.path
import sys

if __name__ == '__main__':
    if os.path.isdir('/home/user/.ssh/host-keys'):
        print('importing host keys from /home/user/.ssh/host-keys')
        run('cp /home/user/.ssh/host-keys/* /etc/ssh/')
        ssh_host_keys_imported = True

        run('chown root:root /etc/ssh/ssh_host_*', log=False)
        run('chmod 700 /etc/ssh/ssh_host_*', log=False)
        run('chmod 744 /etc/ssh/ssh_host_*.pub', log=False)

    run('service ssh start')

    if os.path.isdir('/home/user/.ssh') and not ssh_host_keys_imported:
        print('exporting host keys from /etc/ssh to /home/user/.ssh/host-keys')
        run('cp /etc/ssh/ssh_host_* /home/user/.ssh/host-keys/')

        run('chown nobody:nobody /home/user/.ssh/host-keys/*', log=False)
        run('chmod 777 /home/user/.ssh/host-keys/*', log=False)
