#!/bin/env python3
from utils import *
import os
import os.path
import sys

if __name__ == '__main__':
    if not exists_dir('/root/.oh-my-zsh'):
        print('moving .oh-my-zsh to /root')
        run('cp -r /home/user.old/.oh-my-zsh /root/')

    if exists_dir('/home/user') and exists_dir('/home/user.old'):
        print('moving stuff from /home/user.old to /home/user...')
        for file in os.listdir('/home/user.old'):
            path = os.path.join('/home/user', file)
            path_old = os.path.join('/home/user.old', file)

            if not os.path.exists(path):
                run(f'cp -pdrx {path_old} /home/user/', log=False)
                print(f'- {file}', colorize('copied', Colors.GREEN))
            else:
                print(f'- {file}', colorize('skipped', Colors.FAINT))
        run('rm -r /home/user.old')
    elif exists_dir('/home/user.old'):
        run('mv /home/user.old /home/user')
