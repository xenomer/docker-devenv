#!/bin/env python3
from utils import *
import os
import os.path
import sys

if __name__ == '__main__':
    if os.environ.get('ROOT_PASSWORD'):
        print('changing root password')
        run('echo "root:$ROOT_PASSWORD" | chpasswd', log=False)

    if os.environ.get('USER_PASSWORD'):
        print('changing user password')
        run('echo "user:$USER_PASSWORD" | chpasswd', log=False)

    if os.environ.get('SUDO_REQUIRES_ROOT_PW') == 'true':
        run('echo "Defaults rootpw" >> /etc/sudoers', log=False)
        print('note that sudo requires root password instead of the user password')

    if os.environ.get('ROOT_SSH') == 'true':
        run('sed -ri \'s/^#?PermitRootLogin\s+.*/PermitRootLogin yes/\' /etc/ssh/sshd_config', log=False)
        print('enabled ssh root access')

    print('changing root shell to zsh')
    run('chsh -s /usr/bin/zsh root')
