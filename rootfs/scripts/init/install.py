#!/bin/env python3
from utils import *
import os
import os.path
import sys


def install_nvm():
    print('installing nvm')
    run('curl -o- -s \
        https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh \
        | PROFILE=/dev/null bash', user='user', stdout=False)


def install_node(version: str):
    if not os.path.isdir('/home/user/.nvm'):
        install_nvm()
        print('installing node')

    run(f'. $HOME/.nvm/nvm.sh \
        && nvm install --no-progress {version} 2>&1', user='user', log=False)


def install_python3():
    run('apt-get install -y python3-venv python3-pip python3-dev', stdout=False)


def install_docker():
    run('curl -fsSL https://download.docker.com/linux/ubuntu/gpg \
        | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg')
    run('echo "deb [arch=$(dpkg --print-architecture) \
        signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] \
        https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" \
        | sudo tee /etc/apt/sources.list.d/docker.list')
    run('apt-get update', stdout=False)
    run('apt-get install -y docker-ce-cli', stdout=False)


if __name__ == '__main__':
    run('apt-get update')
    if os.environ.get('INSTALL'):
        programs = os.environ['INSTALL'].split(' ')
        print('installing programs')

        for program in programs:
            print(colorize(f'install {program}', Colors.BLUE))
            try:
                if program.startswith('node-'):
                    install_node(program[5:])
                elif program == 'python3':
                    install_python3()
                elif program == 'docker':
                    install_docker()
                else:
                    run(f'apt-get install -y {program}', stdout=False)
            except Exception as e:
                print(colorize(f'failed to install {program}', Colors.RED))

    if os.environ.get('ENABLE_VSCODE_TUNNEL') == 'true':
        print(colorize('installing vscode-cli...', Colors.BLUE))
        run('curl -fsSL "https://code.visualstudio.com/sha/download?build=stable&os=cli-alpine-x64" -o /tmp/vscode-cli.tar.gz')
        print('extracting...')
        run('tar -xzf /tmp/vscode-cli.tar.gz -C /tmp')
        run('mv /tmp/code /usr/local/bin/')
        print('vscode-cli installed')
