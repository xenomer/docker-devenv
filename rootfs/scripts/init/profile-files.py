#!/bin/env python3
from utils import *
import os.path

profile_files = [
    '/home/user/.zshrc',
    '/home/user/.bashrc',
]
profile_root_files = [
    '/root/.zshrc',
    '/root/.bashrc',
]

if __name__ == "__main__":
    if not os.path.isdir('/root/.zshrc'):
        print('creating /root/.zshrc')
        command('touch /root/.zshrc')

    config_tag = 'devenv config'
    options_tag = 'devenv options'
    nvm_tag = 'nvm'

    # user files
    #

    with open('/etc/devenv/profile', 'r') as f:
        src_content = f.read()
        src_config_content = get_tag_content(
            src_content, config_tag)
        src_options_content = get_tag_content(
            src_content, options_tag)
        src_nvm_content = get_tag_content(
            src_content, nvm_tag)

    for full_path in profile_files:
        file = os.path.basename(full_path)
        if os.path.exists(full_path):
            with open(full_path, 'r+') as f:
                content = f.read()

                # update options
                new_content, had_options_tag = update_tag_content(
                    content, options_tag, src_options_content, replace=False)

                # update config
                new_content, had_config_tag = update_tag_content(
                    new_content, config_tag, src_config_content)

                # add nvm init commands
                if os.path.exists('/home/user/.nvm'):
                    # update config
                    new_content, had_nvm_tag = update_tag_content(
                        new_content, 'nvm', src_nvm_content, replace=False)
                    if not had_nvm_tag:
                        print(
                            f'added {colorize("nvm", Colors.BLUE)} block to {colorize(full_path, Colors.GREEN)}')

                f.seek(0)
                f.write(new_content)
                f.truncate()

                if not had_options_tag:
                    print(
                        f'added {colorize("options", Colors.BLUE)} block to {colorize(full_path, Colors.GREEN)}')

                if had_config_tag:
                    print(
                        f'updated {colorize("config", Colors.BLUE)} in {colorize(full_path, Colors.GREEN)}')
                else:
                    print(
                        f'added {colorize("config", Colors.BLUE)} block to {colorize(full_path, Colors.GREEN)}')
        else:
            print(
                f'file {colorize(full_path, Colors.GREEN)} does not exist, skipping')

    # root files
    #

    with open('/etc/devenv/profile_root', 'r') as f:
        src_content = f.read()
        src_config_content = get_tag_content(
            src_content, config_tag)
        src_options_content = get_tag_content(
            src_content, options_tag)
        src_nvm_content = get_tag_content(
            src_content, nvm_tag)

    for full_path in profile_root_files:
        file = os.path.basename(full_path)
        if os.path.exists(full_path):
            with open(full_path, 'r+') as f:
                content = f.read()

                # update options
                new_content, had_options_tag = update_tag_content(
                    content, options_tag, src_options_content, replace=False)

                # update config
                new_content, had_config_tag = update_tag_content(
                    new_content, config_tag, src_config_content)

                f.seek(0)
                f.write(new_content)

                if not had_options_tag:
                    print(
                        f'added {colorize("options", Colors.BLUE)} block to {colorize(full_path, Colors.GREEN)}')

                if had_config_tag:
                    print(
                        f'updated {colorize("config", Colors.BLUE)} in {colorize(full_path, Colors.GREEN)}')
                else:
                    print(
                        f'added {colorize("config", Colors.BLUE)} block to {colorize(full_path, Colors.GREEN)}')
        else:
            print(
                f'file {colorize(full_path, Colors.GREEN)} does not exist, skipping')
