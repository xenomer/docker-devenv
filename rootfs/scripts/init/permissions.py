#!/bin/env python3
from utils import *
import os
import os.path
import sys

if __name__ == '__main__':
    print('/projects')
    run('mkdir -p /projects', log=False)
    run('chown user:user /projects', log=False)
    run('chmod 744 /projects', log=False)

    print('/home/user')
    run('chown user:user /home/user', log=False)
    run('chmod 700 /home/user', log=False)
    run('chmod -R 700 /home/user/.cache', log=False)

    print('/usr/local/sbin')
    run('chmod 755 /usr/local/sbin/dev', log=False)
    run('chmod 755 /usr/local/sbin/artifact', log=False)
    run('chmod 755 /usr/local/sbin/init_spaceship_devenv', log=False)

    print('/home/user/.ssh')
    run('chmod 700 /home/user/.ssh', stdout=False, log=False, allow_failure=True)
    run('chown -R user:user /home/user/.ssh',
        stdout=False, log=False, allow_failure=True)
    run('chmod 600 /home/user/.ssh/authorized_keys',
        stdout=False, log=False, allow_failure=True)
