#!/command/with-contenv zsh
set -e

if [[ "$ENABLE_VSCODE_TUNNEL" == "true" ]]; then
    cd /projects
    VSCODE_DATA_DIR=/home/user/.vscode-cli
    if [[ ! -f /usr/local/bin/code ]]; then
        >&2 echo vscode-cli not found!
        sleep infinity
    fi
    if [[ -n "$VSCODE_TUNNEL_NAME" ]]; then
        sudo -u user /usr/local/bin/code tunnel --accept-server-license-terms --cli-data-dir=$VSCODE_DATA_DIR --name="$VSCODE_TUNNEL_NAME"
    else
        sudo -u user /usr/local/bin/code tunnel --accept-server-license-terms --cli-data-dir=$VSCODE_DATA_DIR --random-name
    fi
fi