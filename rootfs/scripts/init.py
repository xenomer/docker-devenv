#!/bin/env python3

from utils import *
import os
import os.path
import sys

print('initializing...')

scripts = [
    'move-home.py',
    'permissions.py',
    'users.py',
    'sshd.py',
    'install.py',
    'profile-files.py',
    'custom.py',
]

for script in scripts:
    script_name = os.path.splitext(script)[0]
    print(colorize(f'- {script_name}', Colors.GREEN))
    try:
        run_script(
            f'/scripts/init/{script}',
            stdout_line_prefix=colorize('  > ', Colors.FAINT),
            stderr_line_prefix=colorize('  > ', Colors.RED)
        )
    except Exception as e:
        msg, code = e.args
        print(f'error: {msg}')
        sys.exit(code)
