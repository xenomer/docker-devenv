#!/bin/env python3
# This file will be run when building the image,
# while the init scripts will be run when the container is started.

from utils import *
import os
import os.path
import sys

if __name__ == "__main__":
    run('touch /home/user/.zsh_history')
    run('rm /home/user/.profile /home/user/.bash_logout')
    run('mkdir -p /home/user/.local/bin')
    run('mkdir -p /home/user/.cache')

    os.environ['ZSH_CUSTOM'] = '/home/user/.oh-my-zsh/custom'

    run('echo $ZSH_CUSTOM')

    print('installing oh-my-zsh')
    run('sh -c \
        "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"')

    if not os.path.isfile('/home/user/.zshrc'):
        print(
            '~/.zshrc not found. There was probably an error installing oh-my-zsh.',
            file=sys.stderr
        )
        sys.exit(1)

    print('installing spaceship-prompt')
    run('git clone https://github.com/denysdovhan/spaceship-prompt.git \
        "$ZSH_CUSTOM/themes/spaceship-prompt" --depth=1')
    run('ln -s "$ZSH_CUSTOM/themes/spaceship-prompt/spaceship.zsh-theme" \
        "$ZSH_CUSTOM/themes/spaceship.zsh-theme"')

    print('installing zsh-autosuggestions')
    run('git clone https://github.com/zsh-users/zsh-autosuggestions \
        ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions')

    print('changing plugins in ~/.zshrc')
    replace_in_file('/home/user/.zshrc',
                    re.compile(r'plugins=\(([a-z0-9\s]+)\)', re.IGNORECASE),
                    r'plugins=(\1 zsh-autosuggestions tmux)')

    print('changing theme in ~/.zshrc')
    replace_in_file('/home/user/.zshrc',
                    re.compile(r'ZSH_THEME=".+"', re.IGNORECASE),
                    'ZSH_THEME="spaceship"')

    print('tmux config in ~/.tmux.conf')
    with open('/home/user/.tmux.conf', 'a') as f:
        f.write('\n')
        f.write('set -g mouse on\n')
        f.write('set-option -g history-limit 25000\n')
